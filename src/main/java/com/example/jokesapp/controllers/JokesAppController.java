package com.example.jokesapp.controllers;

import com.example.jokesapp.services.JokeProvider;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class JokesAppController {

  JokeProvider jokeProvider;

  public JokesAppController(JokeProvider jokeProvider) {
    this.jokeProvider = jokeProvider;
  }

  @GetMapping("/")
  String getHome(Model model) {
    model.addAttribute("joke", jokeProvider.generateRandomJoke());
    return "main";
  }
}
