package com.example.jokesapp.services.impl;

import com.example.jokesapp.services.JokeProvider;
import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.springframework.stereotype.Service;

@Service
public class JokeProviderImpl implements JokeProvider {

  ChuckNorrisQuotes chuckNorrisQuotes;

  public JokeProviderImpl(ChuckNorrisQuotes chuckNorrisQuotes) {
    this.chuckNorrisQuotes = chuckNorrisQuotes;
  }

  @Override
  public String generateRandomJoke() {
    chuckNorrisQuotes = new ChuckNorrisQuotes();
    return chuckNorrisQuotes.getRandomQuote();
  }
}
