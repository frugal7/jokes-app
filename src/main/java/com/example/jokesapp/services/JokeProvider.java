package com.example.jokesapp.services;

public interface JokeProvider {
  String generateRandomJoke();
}
